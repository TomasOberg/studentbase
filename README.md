# README #

A flight booking prototype built in JSF/PrimeFaces and Angular.io

### What is this repository for? ###

This prototype built for the purpose of illustrating differences between a server and client side generated web application.

### How do I get set up? ###

* Install Wildfly 
* Install MySQL
* Install Maven
* Install Angular CLI
* Run ConfigWildfly.cl.("jboss-cli.sh --file=ConfigWildfly.cli")
* Run each of the .sql-files in the config in order to set up the db
* Install through maven (mvn install)
* Startup the wildfly server either using you preferable IDE or through manual deploument
* JSF Application found at localhost:8080/TicketBooking
* Angular.io application found at localhost:8080/TicketBooking/ng
(or at localhost:4200 if "ng server" is started in in the studentbase/src/main/ng directory)

### Who do I talk to? ###

* Contact tomaserikoberg@gmail.com for any questions