package com.cgi.osd.transactiondemo.logic.DTO;

/**
 * Created by tomasoberg on 2017-04-04.
 */
public class SeatsRequestDTO {

    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
