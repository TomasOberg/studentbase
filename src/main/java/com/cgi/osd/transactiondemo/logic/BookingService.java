package com.cgi.osd.transactiondemo.logic;

import java.util.Date;
import java.util.List;

import com.cgi.osd.transactiondemo.logic.domainobject.*;
import com.cgi.osd.transactiondemo.logic.exception.PersistenceOperationException;
import com.cgi.osd.transactiondemo.logic.DTO.RestPaymentResponseDTO;
import com.cgi.osd.transactiondemo.persistence.model.Transaction;

/**
 * This interface defines the method required for handling bookings.
 */
public interface BookingService {

    List<AirportDO> getAllAirports() throws PersistenceOperationException;

    boolean reserveSeat(int seatNr, int flight, CredentialsDO credentials) throws PersistenceOperationException;

    List<FlightDO> findFlightsBetweenDates(AirportDO departure, AirportDO destination, Date date1, Date date2) throws PersistenceOperationException;

    FlightDO getFlight(int nr) throws PersistenceOperationException;

    AirportDO getAirportById(int id) throws PersistenceOperationException;

    List<SeatDO> getSeatsWithBookingsForFlight(int flight) throws PersistenceOperationException;

    RestPaymentResponseDTO makePayment(Integer cardnumber, String name, Double amount);

    List<AirportDO> getAirport(String name) throws PersistenceOperationException;

    void storeTransaction(Transaction transaction) throws PersistenceOperationException;

    public List<BookingDO> getAllBookings() throws PersistenceOperationException;

}

