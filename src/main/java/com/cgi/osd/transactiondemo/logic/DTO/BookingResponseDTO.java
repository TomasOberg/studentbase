package com.cgi.osd.transactiondemo.logic.DTO;

/**
 * Created by tomasoberg on 2017-04-06.
 */
public class BookingResponseDTO {

    private String responseText;

    public BookingResponseDTO(String responseText) {
        this.responseText = responseText;
    }

    public String getResponseText() {
        return responseText;
    }
}
