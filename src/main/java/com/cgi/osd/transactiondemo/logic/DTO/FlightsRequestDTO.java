package com.cgi.osd.transactiondemo.logic.DTO;

import java.util.Date;

/**
 * Created by tomasoberg on 2017-04-04.
 */

public class FlightsRequestDTO {
    private Date departureDate;
    private Date arrivalDate;
    private Integer departure;
    private Integer destination;

    private String price;

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }
    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }
    public void setDeparture(Integer departure) {
        this.departure = departure;
    }
    public void setDestination(Integer destination) {
        this.destination = destination;
    }

    public Date getDepartureDate() {
        return departureDate;
    }
    public Date getArrivalDate() {
        return arrivalDate;
    }
    public Integer getDeparture() {
        return departure;
    }
    public Integer getDestination() {
        return destination;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
