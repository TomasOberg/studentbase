package com.cgi.osd.transactiondemo.logic.DTO;

import com.cgi.osd.transactiondemo.logic.domainobject.CredentialsDO;
import com.cgi.osd.transactiondemo.logic.domainobject.FlightDO;

/**
 * Created by tomasoberg on 2017-04-06.
 */
public class BookingRequestDTO {

    private int seatNr;
    private int flight;
    private CredentialsDO credentials;

    private String price;

    public int getSeatNr() {
        return seatNr;
    }

    public void setSeatNr(int seatNr) {
        this.seatNr = seatNr;
    }

    public int getFlight() {
        return flight;
    }

    public void setFlight(int flight) {
        this.flight = flight;
    }

    public CredentialsDO getCredentials() {
        return credentials;
    }

    public void setCredentials(CredentialsDO credentials) {
        this.credentials = credentials;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
