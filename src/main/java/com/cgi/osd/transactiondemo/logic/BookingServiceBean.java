package com.cgi.osd.transactiondemo.logic;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.cgi.osd.transactiondemo.logic.domainobject.*;
import com.cgi.osd.transactiondemo.logic.exception.PersistenceOperationException;
import com.cgi.osd.transactiondemo.persistence.PersistenceManager;
import com.cgi.osd.transactiondemo.logic.DTO.CardInfoDTO;
import com.cgi.osd.transactiondemo.logic.DTO.RestPaymentResponseDTO;
import com.cgi.osd.transactiondemo.persistence.model.Transaction;
import com.cgi.osd.transactiondemo.presentation.rest.RestPaymentService;

/**
 * This class is responsible for implementing the booking services.
 */

@ApplicationScoped
public class BookingServiceBean implements BookingService {


    @Inject
    private PersistenceManager persistenceManager;

    @Inject
    private RestPaymentService paymentService;

    @Override
    public List<AirportDO> getAllAirports() throws PersistenceOperationException {
        final List<AirportDO> airports = this.persistenceManager.getAllAirports();
        return airports;
    }

    @Override
    public boolean reserveSeat(int seatNr, int flight, CredentialsDO credentials) throws PersistenceOperationException {
        final boolean result = this.persistenceManager.reserveSeat(seatNr, flight, credentials);
        return result;
    }

    @Override
    public List<FlightDO> findFlightsBetweenDates(AirportDO departure, AirportDO destination, Date date1, Date date2) throws PersistenceOperationException {
        final List<FlightDO> bookings = this.persistenceManager.findFlightsBetweenDates(departure, destination, date1, date2);
        return bookings;
    }
    @Override
    public FlightDO getFlight(int nr) throws PersistenceOperationException {
        final FlightDO flight = this.persistenceManager.findFlight(nr);
        return flight;
    }

    @Override
    public AirportDO getAirportById(int id) throws PersistenceOperationException {
        final AirportDO airportDO = this.persistenceManager.getAirportById(id);
        return airportDO;
    }

    @Override
    public List<SeatDO> getSeatsWithBookingsForFlight(int flight) throws PersistenceOperationException {
        final List<SeatDO> seats = this.persistenceManager.getSeatsWithBookingsForFlight(flight);
        return seats;
    }

    @Override
    public RestPaymentResponseDTO makePayment(Integer cardnumber, String name, Double amount) {
        final RestPaymentResponseDTO restPaymentResponseDTO =
                paymentService.performPayment(new CardInfoDTO(cardnumber, name, amount, UUID.randomUUID().toString()));
        return restPaymentResponseDTO;
    }

    @Override
    public void storeTransaction(Transaction transaction) throws PersistenceOperationException {
        this.persistenceManager.storeTransaction(transaction);
    }

    @Override
    public List<AirportDO> getAirport(String name) throws PersistenceOperationException {
        return this.persistenceManager.getAirport(name);
    }

    @Override
    public List<BookingDO> getAllBookings() {
        return this.persistenceManager.getAllBookings();
    }
}
