package com.cgi.osd.transactiondemo.logic.domainobject;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This class is responsible for implementing the domain object Flight.
 */
public class FlightDO {

    private final Integer id;

    private final int departure;
    private final int destination;

    private final Date departureTime;
    private final Date arrivalTime;

    private final String departureTimeFormatted;
    private final String arrivalTimeFormatted;

    private final double price;

    public FlightDO(int id, int departure, int destination, Date departureTime, Date arrivalTime, double price) {
        this.id = id;
        this.departure = departure;
        this.destination = destination;
        this.departureTime = departureTime;
        this.arrivalTime = arrivalTime;
        this.price = price;

        SimpleDateFormat frm = new SimpleDateFormat("hh:mm");

        this.departureTimeFormatted = frm.format(getDepartureTime());
        this.arrivalTimeFormatted = frm.format(getArrivalTime());

    }

    public FlightDO(int id, Date departureTime, Date arrivalTime, int departure, int destination, double price,
                    String departureTimeFormatted, String arrivalTimeFormatted)
    {
        this.id = id;
        this.departure = departure;
        this.destination = destination;
        this.departureTime = departureTime;
        this.arrivalTime = arrivalTime;
        this.price = price;

        this.departureTimeFormatted = departureTimeFormatted;
        this.arrivalTimeFormatted = arrivalTimeFormatted;

    }
    public Integer getId() {
        return id;
    }

    public int getDeparture() {
        return departure;
    }

    public int getDestination() {
        return destination;
    }

    public Date getDepartureTime() {
        return departureTime;
    }

    public Date getArrivalTime() {
        return arrivalTime;
    }


    @Override
    public String toString() {
        SimpleDateFormat frm = new SimpleDateFormat("hh:mm");
        StringBuilder text = new StringBuilder();
        text.append(Integer.toString(getId()));
        text.append(frm.format(getDepartureTime()));
        text.append(" -> ");
        text.append(frm.format(getArrivalTime()));
        return text.toString();
    }


    // Needed for the FlightDOConverter
    @Override
    public boolean equals(Object other) {
        return (other != null && getClass() == other.getClass() && id != null)
                ? id.equals(((FlightDO) other).getId())
                : (other == this);
    }

    // Needed for the FlightDOConverter
    @Override
    public int hashCode() {
        return (id != null)
                ? (getClass().hashCode() + id.hashCode())
                : super.hashCode();
    }

    public String getDepartureTimeFormatted() {
        return departureTimeFormatted;
    }

    public String getArrivalTimeFormatted() {
        return arrivalTimeFormatted;
    }

    public double getPrice() {
        return price;
    }


}
