package com.cgi.osd.transactiondemo.logic.DTO;

import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;

import java.util.List;

/**
 * Created by tomasoberg on 2017-04-04.
 */
public class SeatsResponseDTO {

    protected List<SeatDO> seats;

    public SeatsResponseDTO(List<SeatDO> seats) {
        this.seats = seats;
    }

    public List<SeatDO> getSeats() {
        return seats;
    }

    public void setSeats(List<SeatDO> seats) {
        this.seats = seats;
    }
}
