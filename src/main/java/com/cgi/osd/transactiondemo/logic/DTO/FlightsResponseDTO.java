package com.cgi.osd.transactiondemo.logic.DTO;

import com.cgi.osd.transactiondemo.logic.domainobject.FlightDO;

import java.util.List;

/**
 * Created by tomasoberg on 2017-04-04.
 */

public class FlightsResponseDTO {

    private final List<FlightDO> flights;

    public FlightsResponseDTO(List<FlightDO> flights) {
        this.flights = flights;
    }
    public List<FlightDO> getFlights() {
        return flights;
    }


}
