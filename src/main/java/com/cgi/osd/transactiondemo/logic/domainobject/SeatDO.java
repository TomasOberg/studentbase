package com.cgi.osd.transactiondemo.logic.domainobject;

import com.cgi.osd.transactiondemo.persistence.model.Booking;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * This class is responsible for implementing the domain object Seat.
 */
public class SeatDO {

    private int seatNumber;
    private boolean reservationStatus;
    private List<BookingDO> bookings = new ArrayList<>();

    public SeatDO(int seatNumber, List<BookingDO> bookings) {
        this.seatNumber = seatNumber;
        this.bookings = bookings;
    }

    public SeatDO(int seatNumber) {
        this.seatNumber = seatNumber;
    }

    public int getSeatNumber() {
        return this.seatNumber;
    }

    public boolean getReservationStatusForFlight(int flight) {
        // Om någon av sätets bokningar överensstämmer med flighten som är vald, returnera att sätet är bokat
        for (BookingDO bookingDO : bookings) {
            if (bookingDO.getFlight() == flight) {
                return true;
            }
        }
        return false;
    }
    public List<BookingDO> getBookings() {
        return bookings;
    }
    public void addBooking(BookingDO bookingDO) {
        bookings.add(bookingDO);
    }
    public void setBookings(List<BookingDO> bookings) {
        this.bookings = bookings;
    }

}
