package com.cgi.osd.transactiondemo.logic.DTO;

/**
 * Created by tomasoberg on 2017-04-07.
 */
public class RestPaymentResponseDTO {

    private Long transactionTime;
    private String requestId;
    private String transactionId;
    private Double amount;

    public void setTransactionTime(Long transactionTime) {
        this.transactionTime = transactionTime;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Long getTransactionTime() {
        return transactionTime;
    }

    public String getRequestId() {
        return requestId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public Double getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(transactionTime).append(" ").append(requestId).append(" ").append(transactionId).append(" ").append(amount);
        return builder.toString();
    }
}
