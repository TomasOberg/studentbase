package com.cgi.osd.transactiondemo.logic.DTO;

import com.cgi.osd.transactiondemo.logic.domainobject.AirportDO;

import java.util.List;

/**
 * Created by tomasoberg on 2017-04-05.
 */
public class AirportsResponseDTO {
    private final List<AirportDO> airports;

    public AirportsResponseDTO(List<AirportDO> airports) {
        this.airports = airports;
    }

    public List<AirportDO> getAirports() {
        return airports;
    }
}
