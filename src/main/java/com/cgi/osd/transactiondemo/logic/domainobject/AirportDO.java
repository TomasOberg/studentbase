package com.cgi.osd.transactiondemo.logic.domainobject;

/**
 * This class is responsible for implementing the domain object Airport.
 */
public class AirportDO {

    private String name;
    private Integer id;

    public AirportDO(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }


    // Needed for the AirportDOConverter
    @Override
    public boolean equals(Object other) {
        return (other != null && getClass() == other.getClass() && id != null)
                ? id.equals(((AirportDO) other).getId())
                : (other == this);
    }

    // Needed for the AirportDOConverter
    @Override
    public int hashCode() {
        return (id != null)
                ? (getClass().hashCode() + id.hashCode())
                : super.hashCode();
    }
}
