package com.cgi.osd.transactiondemo.logic.domainobject;

import javax.validation.constraints.Pattern;

/**
 * This class is responsible for implementing the domain object Credentials.
 */
public class CredentialsDO {

    private String firstname;
    private String lastname;
    private Integer age;
    private String street;
    private String city;
    private Integer postalCode;


    private String email;

    private String phone;

    // TODO: Put these in a separate class?
    private String creditcard_payer;
    private String creditcard;

    public CredentialsDO() {
    }

    public CredentialsDO(String firstname, String lastname, Integer age, String street, String city, Integer postalCode, String email, String phone, String creditcard_payer, String creditcard) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.age = age;
        this.street = street;
        this.city = city;
        this.postalCode = postalCode;
        this.email = email;
        this.phone = phone;
        this.creditcard_payer = creditcard_payer;
        this.creditcard = creditcard;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(Integer postalCode) {
        this.postalCode = postalCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCreditcard_payer() {
        return creditcard_payer;
    }

    public String getCreditcard() {
        return creditcard;
    }

    public void setCreditcard_payer(String creditcard_payer) {
        this.creditcard_payer = creditcard_payer;
    }

    public void setCreditcard(String creditcard) {
        this.creditcard = creditcard;
    }

    public String creditcardFiltered() throws Exception {
        if (creditcard.length() - 4 <= 0)
            throw new Exception("Exception Credit card length is too small!");
        return "****-****-****-" + creditcard.substring(creditcard.length() - 4, creditcard.length());
    }

    public void clear() {
        this.firstname = null;
        this.lastname = null;
        this.age = null;
        this.street = null;
        this.city = null;
        this.postalCode = null;
        this.email = null;
        this.phone = null;
        this.creditcard_payer = null;
        this.creditcard = null;
    }
}
