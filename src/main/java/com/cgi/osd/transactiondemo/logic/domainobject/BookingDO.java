package com.cgi.osd.transactiondemo.logic.domainobject;

import com.cgi.osd.transactiondemo.persistence.model.Credentials;
import com.cgi.osd.transactiondemo.persistence.model.Flight;

import javax.persistence.Column;

/**
 * This class is responsible for implementing the domain object Booking.
 */
public class BookingDO {

    private int flight;
    private int seat;
    private String email;
    private CredentialsDO credentialsDO;

    public BookingDO(int flight, int seat, String email) {
        this.flight = flight;
        this.seat = seat;
        this.email = email;

    }

    public int getFlight() {
        return flight;
    }

    public int getSeat() {
        return seat;
    }

    public String getCustomer() {
        return email;
    }

    public CredentialsDO getCredentialsDO() {
        return credentialsDO;
    }

    public void setCredentialsDO(CredentialsDO credentialsDO) {
        this.credentialsDO = credentialsDO;
    }


}
