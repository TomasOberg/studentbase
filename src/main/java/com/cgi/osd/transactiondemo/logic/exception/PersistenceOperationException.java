package com.cgi.osd.transactiondemo.logic.exception;

import java.io.Serializable;

/**
 * This class is used for representing persistence exceptions that are not lock exceptions.
 *
 */
public class PersistenceOperationException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public PersistenceOperationException(String message) {
	    super(message);
    }

}
