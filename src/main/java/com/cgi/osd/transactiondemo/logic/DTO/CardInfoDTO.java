package com.cgi.osd.transactiondemo.logic.DTO;

/**
 * Created by tomasoberg on 2017-04-07.
 */
public class CardInfoDTO {

    private Integer cardNumber;
    private String cardHolderName;
    private Double amount;
    private String requestId;

    public CardInfoDTO(Integer cardNumber, String cardHolderName, Double amount, String requestId) {
        this.cardNumber = cardNumber;
        this.cardHolderName = cardHolderName;
        this.amount = amount;
        this.requestId = requestId;
    }

    public Integer getCardNumber() {
        return cardNumber;
    }

    public String getCardHolderName() {
        return cardHolderName;
    }

    public Double getAmount() {
        return amount;
    }

    public String getRequestId() {
        return requestId;
    }
}
