package com.cgi.osd.transactiondemo.presentation.rest;

import com.cgi.osd.transactiondemo.logic.DTO.CardInfoDTO;
import com.cgi.osd.transactiondemo.logic.DTO.RestPaymentResponseDTO;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.mail.Session;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

/**
 * Created by tomasoberg on 2017-04-06.
 */

public class RestPaymentService {

    @Inject
    FacesContext facesContext;

    public RestPaymentResponseDTO performPayment(CardInfoDTO cardinfoDTO) {

        // Payment service at http://studentpayment-cgiostersund.rhcloud.com/paymentserver/rest/paymentservices/pay
        // stored as setting in the app server
        final String paymentservice = System.getProperty("paymentservice");

        RestPaymentResponseDTO restResponseDTO = null;
        try {
            final ResteasyWebTarget target = RestPaymentClient.getRestPaymentClient().setup( paymentservice );
            final Response response = target.request().post(Entity.entity(cardinfoDTO, "application/json"));
            if (response.getStatus() != 200) {
                throw new RuntimeException("Failed: HTTP error code: " + response.getStatus());

                // TODO: GROWL MESSAGE
                // facesContext.addMessage();

            }
            restResponseDTO = response.readEntity(RestPaymentResponseDTO.class);
            response.close();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        return restResponseDTO;
    }
}
