package com.cgi.osd.transactiondemo.presentation.rest.exception;

import com.cgi.osd.transactiondemo.logic.exception.PersistenceOperationException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created by tomasoberg on 2017-04-13.
 */

@Provider
public class PersistenceOperationExceptionHandler implements ExceptionMapper<PersistenceOperationException> {

    @Override
    public Response toResponse(PersistenceOperationException exception) {
        final ErrorMessage errorMessage = new ErrorMessage(500, exception.getMessage() );
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity( errorMessage ).build();
    }
}
