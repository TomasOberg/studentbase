package com.cgi.osd.transactiondemo.presentation.rest;

/**
 * Created by tomasoberg on 2017-03-31.
 */

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/rest")
public class RestApplication extends Application {

}
