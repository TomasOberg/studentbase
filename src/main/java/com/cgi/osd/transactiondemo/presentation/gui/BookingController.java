package com.cgi.osd.transactiondemo.presentation.gui;

import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.enterprise.context.SessionScoped;
import javax.annotation.PostConstruct;
import javax.interceptor.Interceptors;
import javax.inject.Inject;
import javax.inject.Named;

import com.cgi.osd.transactiondemo.logic.BookingService;
import com.cgi.osd.transactiondemo.logic.DTO.RestPaymentResponseDTO;
import com.cgi.osd.transactiondemo.logic.domainobject.*;
import com.cgi.osd.transactiondemo.logic.exception.PersistenceOperationException;
import com.cgi.osd.transactiondemo.persistence.ExceptionInterceptor;
import com.cgi.osd.transactiondemo.persistence.model.Transaction;
import com.cgi.osd.transactiondemo.util.AirportDOConverter;
import com.cgi.osd.transactiondemo.util.MailService;
import org.primefaces.event.FlowEvent;

/**
 * This class is responsible for the controller part of the MVC pattern for the booking process.
 */
@Named
@SessionScoped
public class BookingController implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private BookingData bookingData;

    @Inject
    private BookingService bookingService;

    @Inject
    private AirportDOConverter airportDOConverter;

    @Inject
    private MailService mailService;

    @Inject
    private Logger logger;

    @Inject
    private FacesContext facesContext;

    Map<String, String> confirmationLetter = new HashMap<>();

    @PostConstruct
    private void init() {
        this.confirmationLetter.clear();
        bookingData.setCurrentDate(new Date());  // Sets the Date to now whenever page reloads
        final List<AirportDO> allAirports = bookingService.getAllAirports();
        bookingData.setAvailableAirports(allAirports);

        System.out.println("ADMIN");

        final List<BookingDO> allBookings = bookingService.getAllBookings();
        for (BookingDO b : allBookings) {
            System.out.println("Bokning: " + b.getCustomer());
        }

    }

    public String onFlowProcess(FlowEvent event) {
        // On every tab change that goes to seatTab, update all the seats array
        if (event.getNewStep().equals("seatTab")) {

            if (bookingData.getChosenFlight() == null) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Flight", "Choose a flight");
                facesContext.addMessage(null, msg);
                return event.getOldStep();
            }
            updateAllSeats();
        } else if (event.getNewStep().equals("credentialsTab")) {
            if (bookingData.getSelectedSeat() < 0) {

                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Seat", "Choose a seat");
                facesContext.addMessage(null, msg);

                return event.getOldStep();
            }
        }


        return event.getNewStep();
    }


    /**
     * Retrieves a list of seats from the DB and stores it in the seats field
     */
    public void updateAllSeats() {
        try {
            logger.fine("uppdaterar säten!");
            final List<SeatDO> seatsWithBookingsForFlight = bookingService.getSeatsWithBookingsForFlight(bookingData.getChosenFlight().getId());
            bookingData.setSeats(seatsWithBookingsForFlight);
        } catch (PersistenceOperationException e) {
            e.printStackTrace();
        }
    }

    public AirportDOConverter getAirportDOConverter() {
        return airportDOConverter;
    }


    public void setCurrentDate(Date date) {
        this.bookingData.setCurrentDate(date);
    }

    public Date getCurrentDate() {
        return this.bookingData.getCurrentDate();
    }

    public AirportDO getCurrentDeparture() {
        return bookingData.getCurrentDeparture();
    }

    public void setCurrentDeparture(AirportDO currentDeparture) {
        this.bookingData.setCurrentDeparture(currentDeparture);
    }

    public AirportDO getCurrentDestination() {
        return bookingData.getCurrentDestination();
    }

    public void setCurrentDestination(AirportDO destination) {
        bookingData.setCurrentDestination(destination);
    }

    public List<SeatDO> getSeats() {
        return this.bookingData.getSeats();
    }

    public CredentialsDO getCredentials() {
        return bookingData.getCredentials();
    }

    public void setCredentials(CredentialsDO credentials) {
        bookingData.setCredentials(credentials);
    }

    public int getSelectedSeat() {
        return bookingData.getSelectedSeat();
    }

    public void setSelectedSeat(int selectedSeat) {
        bookingData.setSelectedSeat(selectedSeat);
    }

    /**
     * Make a reservation for a seat number
     *
     * @param seatNr - integer, the seat to be reserved
     */
    public void reserve(int seatNr) {
        bookingData.setSelectedSeat(seatNr);
        FacesMessage msg = new FacesMessage("Seat", "Chosen seat :" + seatNr);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }


    public List<FlightDO> getAvailableFlights() {
        return bookingData.getAvailableFlights();
    }

    public FlightDO getChosenFlight() {
        return bookingData.getChosenFlight();
    }

    public void setChosenFlight(FlightDO chosenFlight) {
        bookingData.setChosenFlight(chosenFlight);
    }

    public boolean reservedForFlight(int seatNr, int flight) {
        if (bookingData.getSeats() != null) {
            return bookingData.getSeats().get(seatNr - 1).getReservationStatusForFlight(flight);
        }
        return false;
    }

    @Interceptors(ExceptionInterceptor.class)
    public List<AirportDO> completeAirport(String query) {
        if (query == null)
            return null;

        List<AirportDO> airportDOS = null;


        try {
            airportDOS = bookingData.getAvailableAirports();
        } catch (Exception e) {
            FacesMessage fmMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "System error", e.getClass().getName());
            FacesContext.getCurrentInstance().addMessage(null, fmMsg);
        }
        List<AirportDO> filteredAirportDOs = new ArrayList<>();
        for (AirportDO eachAirport : airportDOS) {
            if (eachAirport.getName().toLowerCase().startsWith(query.toLowerCase())) {
                filteredAirportDOs.add(eachAirport);
            }
        }
        return filteredAirportDOs;
    }

    public void updateAvailableFlights() {


        if (bookingData.getCurrentDeparture() == null) {
            FacesMessage msg = new FacesMessage("Travel", "Chose a departure");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            logger.info("Departure is null");
            return;
        }

        if (bookingData.getCurrentDestination() == null) {
            FacesMessage msg = new FacesMessage("Travel", "Chosen a destination");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            logger.info("Destination is null");
            return;
        }

        if (bookingData.getCurrentDate() == null) {
            FacesMessage msg = new FacesMessage("Travel", "Chosen a date");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            logger.info("Date is null");
            return;

        }

        //---------------------------------------------------------------------
        // As JPQL doesn't support DATE() casting in its queries we need to
        // create an interval to search between instead. Therefore
        // today's date and time is taken and added 24 hours ahead and assigned
        // to the end of the range
        //---------------------------------------------------------------------

        Date date2 = bookingData.getCurrentDate();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(bookingData.getCurrentDate());
        calendar.add(Calendar.HOUR_OF_DAY, 24);
        date2 = calendar.getTime();

        logger.info(String.valueOf(bookingData.getCurrentDate()));
        logger.info(" TILL: ");
        logger.info(String.valueOf(date2));

        List<FlightDO> availableFlights = bookingService.findFlightsBetweenDates(bookingData.getCurrentDeparture(), bookingData.getCurrentDestination(), bookingData.getCurrentDate(), date2);
        bookingData.setAvailableFlights(availableFlights);

    }

    public void book() {

        try {
            final CredentialsDO credentials = bookingData.getCredentials();
            final FlightDO chosenFlight = bookingData.getChosenFlight();
            final AirportDO currentDeparture = bookingData.getCurrentDeparture();
            final AirportDO currentDestination = bookingData.getCurrentDestination();


            //-----------------------------------------------------------------
            // Special case here, we just use the first four digits in the card
            // number for this special card service.
            //-----------------------------------------------------------------
            Transaction transaction = null;
            final String[] firstFourDigits = credentials.getCreditcard().split("-");
            if (firstFourDigits[0] != null) {

                int cardnumber = Integer.parseInt(firstFourDigits[0]);

                final RestPaymentResponseDTO restPaymentResponseDTO = bookingService.makePayment(
                        cardnumber,
                        credentials.getCreditcard_payer(),
                        chosenFlight.getPrice()
                );

                logger.info("RESULT: " + restPaymentResponseDTO);


                if (restPaymentResponseDTO == null) {
                    FacesMessage msg = new FacesMessage("Unsuccessful", " Transaction failed");
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                    return;
                }

                transaction = new Transaction();
                transaction.setTransactionId(restPaymentResponseDTO.getTransactionId());
                transaction.setRequestId(restPaymentResponseDTO.getRequestId());
                transaction.setAmount(restPaymentResponseDTO.getAmount());
                transaction.setTransactionTime(restPaymentResponseDTO.getTransactionTime());
                transaction.setEmail(credentials.getEmail());

                bookingService.storeTransaction(transaction);
            } else {
                // TODO: Fail... make message about wrong cardnumber... should not happen
                return;
            }

            final boolean result = bookingService.reserveSeat(bookingData.getSelectedSeat(), chosenFlight.getId(), credentials);
            if (!result) {
                FacesMessage msg = new FacesMessage("Unsuccessful", "Seat " + bookingData.getSelectedSeat() + " not booked for :" + credentials.getFirstname());
                FacesContext.getCurrentInstance().addMessage(null, msg);
                return;
            }

            String confirmationLetter = composeConfirmationLetter(currentDeparture, currentDestination, chosenFlight, credentials);
            mailService.sendMail(credentials.getEmail(), confirmationLetter);

            // TODO: Use template for mailing instead and use the same on the confirmation page.
            facesContext.getExternalContext().redirect("confirmation.jsf");
            bookingData.clearAll();


        } catch (PersistenceOperationException | IOException e) {
            e.printStackTrace();
        }
    }

    private String composeConfirmationLetter(AirportDO currentDeparture, AirportDO currentDestination, FlightDO chosenFlight, CredentialsDO credentials) {
        SimpleDateFormat frm = new SimpleDateFormat("YYYY-MM-dd");

        String depDate = frm.format(chosenFlight.getDepartureTime());
        String arrDate = frm.format(chosenFlight.getArrivalTime());

        StringBuilder confirmationLetter = new StringBuilder();
        confirmationLetter.append("Dear ").append(credentials.getFirstname()).append(" ").append(credentials.getLastname()).append("!\n\n");
        confirmationLetter.append("Thank you for your booking!").append("\n");
        confirmationLetter.append("Seat " + bookingData.getSelectedSeat() + " booked for flight: " + chosenFlight.getId() + ", ");
        confirmationLetter.append(currentDeparture.getName() + " bound for " + currentDestination.getName() + " ");
        confirmationLetter.append("on: " + depDate + " " + chosenFlight.getDepartureTimeFormatted() + "\n");
        confirmationLetter.append("arrival: " + arrDate + " " + chosenFlight.getArrivalTimeFormatted() + "\n");

        this.confirmationLetter.put("firstName", credentials.getFirstname());
        this.confirmationLetter.put("lastName", credentials.getLastname());
        this.confirmationLetter.put("seat", String.valueOf(bookingData.getSelectedSeat()));
        this.confirmationLetter.put("flight", String.valueOf(chosenFlight.getId()));
        this.confirmationLetter.put("departureName", currentDeparture.getName());
        this.confirmationLetter.put("arrivalName", currentDestination.getName());
        this.confirmationLetter.put("departureDate", depDate);
        this.confirmationLetter.put("arrivalDate", arrDate);
        this.confirmationLetter.put("departureTime", chosenFlight.getDepartureTimeFormatted());
        this.confirmationLetter.put("arrivalTime", chosenFlight.getArrivalTimeFormatted());
        this.confirmationLetter.put("mail", credentials.getEmail());

        return confirmationLetter.toString();
    }


    public Map<String, String> getConfirmationLetter() {
        return confirmationLetter;
    }

    public void setConfirmationLetter(Map<String, String> confirmationLetter) {
        this.confirmationLetter = confirmationLetter;
    }
}



