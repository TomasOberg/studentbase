package com.cgi.osd.transactiondemo.presentation.rest;


import com.cgi.osd.transactiondemo.logic.BookingService;
import com.cgi.osd.transactiondemo.logic.domainobject.AirportDO;
import com.cgi.osd.transactiondemo.logic.domainobject.CredentialsDO;
import com.cgi.osd.transactiondemo.logic.domainobject.FlightDO;
import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;
import com.cgi.osd.transactiondemo.logic.DTO.*;
import com.cgi.osd.transactiondemo.persistence.model.Transaction;
import com.cgi.osd.transactiondemo.util.MailService;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
* Created by tomasoberg on 2017-03-31.
*/

@Path("/FlightBookingService")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class FlightBookingService {

    @Inject
    BookingService bookingService;

    @Inject
    Logger logger;

    @Inject
    MailService mailService;

    @GET
    @Path("/getAirports")
    public AirportsResponseDTO getAirports() {
        List<AirportDO> airports = bookingService.getAllAirports();
        return new AirportsResponseDTO(airports);
    }

    @POST
    @Path("/getAvailableFlights")
    public FlightsResponseDTO getAvailableFlights(FlightsRequestDTO request) {
        AirportDO depDO = null, desDO = null;
            depDO = bookingService.getAirportById(request.getDeparture());
            desDO = bookingService.getAirportById(request.getDestination());
            if (depDO != null && desDO != null) {
                final List<FlightDO> flights = bookingService.findFlightsBetweenDates(depDO, desDO, request.getDepartureDate(), request.getArrivalDate());
                logger.log(Level.INFO, flights.size() + " flighter funna!");
                return new FlightsResponseDTO(flights);
            }
        return null;
    }

    @GET
    @Path("/getSeatsForFlight/{id}")
    public SeatsResponseDTO getSeatsForFlight(@PathParam("id") int id) {
        logger.log(Level.INFO, "Fick flight: " +  id);
        List<SeatDO> seats = bookingService.getSeatsWithBookingsForFlight( id );
        return new SeatsResponseDTO(seats);
    }


    @POST
    @Path("/makeReservation")
    public BookingResponseDTO makeReservation(BookingRequestDTO request) {

        Transaction transaction = null;

        final CredentialsDO credentials = request.getCredentials();
        final FlightDO chosenFlight = bookingService.getFlight( request.getFlight() );
        final AirportDO currentDeparture = bookingService.getAirportById(chosenFlight.getDeparture());
        final AirportDO currentDestination = bookingService.getAirportById(chosenFlight.getDestination());


        //---------------------------------------------------------------------
        // Only use the four first numbers in the creditcard number for
        // compatibility with the payment service (which only accepts four)
        //---------------------------------------------------------------------
        final String[] firstFourDigits = request.getCredentials().getCreditcard().split("-");
        if(firstFourDigits[0] != null) {
            int cardnumber = Integer.parseInt(firstFourDigits[0]);

            final RestPaymentResponseDTO restPaymentResponseDTO = bookingService.makePayment(
                    cardnumber,
                    request.getCredentials().getCreditcard_payer(),
                    Double.parseDouble(request.getPrice())
            );

            logger.info("RESULT: " + restPaymentResponseDTO);
            if(restPaymentResponseDTO == null) {
                logger.log(Level.INFO, "Transaction failed");
                return new BookingResponseDTO("TRANSACTION FAILED");
            }


            transaction = new Transaction();
            transaction.setTransactionId(restPaymentResponseDTO.getTransactionId());
            transaction.setRequestId(restPaymentResponseDTO.getRequestId());
            transaction.setAmount(restPaymentResponseDTO.getAmount());
            transaction.setTransactionTime(restPaymentResponseDTO.getTransactionTime());
            transaction.setEmail(request.getCredentials().getEmail());
            bookingService.storeTransaction( transaction );
        }

        if(bookingService.reserveSeat(request.getSeatNr(), request.getFlight(), request.getCredentials())) {
            logger.log(Level.INFO, "Booking succeed");

            StringBuilder confirmationLetter = new StringBuilder();
            confirmationLetter.append("Dear ").append(request.getCredentials().getFirstname() ).append(" ").append(request.getCredentials().getLastname()).append("!\n\n");
            confirmationLetter.append("Thank you for your booking!").append("\n");
            confirmationLetter.append("Seat " + request.getSeatNr() + " booked for flight: " + chosenFlight.getId() + ", ");
            confirmationLetter.append(currentDeparture.getName() + " bound for " + currentDestination.getName() + " ");
            confirmationLetter.append("on " + chosenFlight.getDepartureTime() + " " + chosenFlight.getDepartureTimeFormatted() + "\n");
            mailService.sendMail( request.getCredentials().getEmail(), confirmationLetter.toString() );

            return new BookingResponseDTO("SUCCEED");
        }
        else {
            logger.log(Level.INFO, "Booking failed");
            return new BookingResponseDTO("FAIL");
        }

    }


}
