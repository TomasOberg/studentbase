package com.cgi.osd.transactiondemo.presentation.rest;

import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by tomasoberg on 2017-04-06.
 */
public class RestPaymentClient {
    private static RestPaymentClient instance;
    private RestPaymentClient() {}

    public static RestPaymentClient getRestPaymentClient() {
        if(instance == null)
            instance = new RestPaymentClient();
        return instance;
    }
    public ResteasyWebTarget setup(String url) {
        ResteasyWebTarget target = null;
        final ResteasyClient client = new ResteasyClientBuilder().build();
        try {
            target = client.target(new URI(url) );
        } catch (URISyntaxException | NullPointerException e) {
            e.printStackTrace();
        }
        return target;
    }
}
