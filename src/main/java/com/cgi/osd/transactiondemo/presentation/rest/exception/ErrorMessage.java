package com.cgi.osd.transactiondemo.presentation.rest.exception;

/**
 * Created by tomasoberg on 2017-04-13.
 */
public class ErrorMessage {
    private int errorCode;
    private String message;

    public ErrorMessage(int errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public String getMessage() {
        return message;
    }
}
