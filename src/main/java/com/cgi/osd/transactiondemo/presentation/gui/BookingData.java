package com.cgi.osd.transactiondemo.presentation.gui;

import java.sql.Time;
import java.util.*;
import java.io.Serializable;
import javax.enterprise.context.Dependent;
import com.cgi.osd.transactiondemo.logic.domainobject.*;

/**
 * This class is responsible for holding booking data that needs to be cached for some reason. Should only be accessed
 * using the BookingController to make sure that only one instance will be connected to session.
 */

@Dependent
public class BookingData implements Serializable {

    private static final long serialVersionUID = 1L;

    private Date currentDate = new Date();
    private AirportDO currentDeparture;
    private AirportDO currentDestination;
    private int selectedSeat = -1;
    private List<SeatDO> seats = null;
    private CredentialsDO credentials = new CredentialsDO();
    private FlightDO chosenFlight;
    private List<FlightDO> availableFlights = new ArrayList<>();
    private Time travelTime;
    private List<AirportDO> availableAirports;

    public Date getCurrentDate() {
        return currentDate;
    }

    public AirportDO getCurrentDestination() {
        return currentDestination;
    }

    public AirportDO getCurrentDeparture() {
        return currentDeparture;
    }

    public void setCurrentDate(Date currentDate) {
        this.currentDate = currentDate;
    }

    public void setCurrentDestination(AirportDO currentDestination) {
        this.currentDestination = currentDestination;
    }

    public void setCurrentDeparture(AirportDO currentDeparture) {
        this.currentDeparture = currentDeparture;
    }

    public CredentialsDO getCredentials() {
        return credentials;
    }

    public void setCredentials(CredentialsDO credentials) {
        this.credentials = credentials;
    }

    public int getSelectedSeat() {
        return selectedSeat;
    }

    public void setSelectedSeat(int selectedSeat) {
        this.selectedSeat = selectedSeat;
    }

    public List<SeatDO> getSeats() {
        return this.seats;
    }

    public void setSeats(List<SeatDO> seats) {
        this.seats = seats;
    }

    public List<FlightDO> getAvailableFlights() {
        return availableFlights;
    }

    public void setAvailableFlights(List<FlightDO> availableFlights) {
        this.availableFlights = availableFlights;
    }

    public FlightDO getChosenFlight() {
        return chosenFlight;
    }

    public void setChosenFlight(FlightDO chosenFlight) {
        this.chosenFlight = chosenFlight;
    }

    public void setAvailableAirports(List<AirportDO> availableAirports) {
        this.availableAirports = availableAirports;
    }

    public List<AirportDO> getAvailableAirports() {
        return availableAirports;
    }

    public void clearAll() {
        credentials.clear();
        selectedSeat = -1;
        chosenFlight = null;
        currentDate = new Date();
        currentDeparture = null;
        currentDestination = null;
        seats = null;
        availableFlights = null;
    }


}
