package com.cgi.osd.transactiondemo.util;

import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import java.io.Serializable;
import java.util.logging.Logger;


/**
 * Created by tomasoberg on 2017-04-24.
 */

@Stateless
public class MailService implements Serializable {

    @Inject
    Logger logger;

    @Resource(name = "java:jboss/mail/gmail")
    private Session session;

    @Asynchronous
    public void sendMail(String receipient, String message) {
        try {
            MimeMessage mimeMessage = new MimeMessage(session);

            Address from = new InternetAddress("cgiticketbooking@gmail.com");
            Address[] to = new InternetAddress[] {new InternetAddress(receipient) };

            mimeMessage.setFrom(from);
            mimeMessage.setRecipients(Message.RecipientType.TO, to );
            mimeMessage.setSubject("Booking receipt");
            mimeMessage.setSentDate(new java.util.Date());
            mimeMessage.setContent(message, "text/plain");

            Transport.send(mimeMessage);

            logger.info("Mail sent to " + receipient);
        }
         catch (javax.mail.MessagingException e)
        {
            e.printStackTrace();
            logger.info("Error in Sending Mail: "+e);
        }

    }

}
