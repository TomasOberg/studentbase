package com.cgi.osd.transactiondemo.util;

import com.cgi.osd.transactiondemo.logic.BookingService;
import com.cgi.osd.transactiondemo.logic.domainobject.FlightDO;
import com.cgi.osd.transactiondemo.logic.exception.PersistenceOperationException;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Created by tomasoberg on 2017-03-27.
 */

@Named
@FacesConverter(forClass = FlightDO.class)
public class FlightDOConverter implements Converter {

    @Inject
    private BookingService bookingService;

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        try {
            final int i = Integer.parseInt(s);
            return bookingService.getFlight(i);
        }
        catch(NumberFormatException | PersistenceOperationException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        if (!(o instanceof FlightDO)) {
            return null;
        }
        final FlightDO o1 = (FlightDO) o;
        try {
            return Integer.toString(o1.getId());
        }
        catch(NumberFormatException e) {
            e.printStackTrace();
            return null;
        }

    }

}

