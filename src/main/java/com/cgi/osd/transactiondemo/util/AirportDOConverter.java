package com.cgi.osd.transactiondemo.util;

import com.cgi.osd.transactiondemo.logic.BookingService;
import com.cgi.osd.transactiondemo.logic.domainobject.AirportDO;
import com.cgi.osd.transactiondemo.logic.exception.PersistenceOperationException;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 * Created by tomasoberg on 2017-03-27.
 */


@Named
@FacesConverter(forClass = AirportDO.class)
public class AirportDOConverter implements Converter, Serializable {

    @Inject
    private BookingService bookingService;

    @Inject
    private FacesContext facesContext;

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        try {
            if(s==null) return null;
            final List<AirportDO> airport = bookingService.getAirport(s);
            if(airport != null && airport.size()>0 ) {
                return airport.get(0);
            }

        } catch (PersistenceOperationException e) {
            FacesMessage fmmsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "System error", "fel!");
            facesContext.addMessage(null, fmmsg);
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        if (!(o instanceof AirportDO)) return null;
        final AirportDO o1 = (AirportDO) o;
        return o1.getName();
    }

}
