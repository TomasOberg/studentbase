package com.cgi.osd.transactiondemo.persistence.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by tomasoberg on 2017-04-07.
 */


@Entity
@Table(name = "transaction")
@NamedQueries({
        @NamedQuery(name = "Transaction.findAll", query = "SELECT s FROM Transaction s order by s.transactionTime"),
        @NamedQuery(name = "Transaction.findById", query = "SELECT s FROM Transaction s WHERE s.transactionId = :transactionId order by s.transactionTime "),

})
public class Transaction extends EntityBase implements Serializable {

    @Column(name = "transaction_time")
    private Long transactionTime;

    @Column(name = "request_id")
    private String requestId;

    @Column(name = "transaction_id")
    private String transactionId;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "email")
    private String email;

    @ManyToOne
    @JoinColumn(name = "booking")
    private Booking booking;


    public Long getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(Long transactionTime) {
        this.transactionTime = transactionTime;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }


    public Booking getBooking() {
        return booking;
    }

    public void setBooking(Booking booking) {
        this.booking = booking;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }
}
