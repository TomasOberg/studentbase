package com.cgi.osd.transactiondemo.persistence.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by tomasoberg on 2017-03-23.
 */


@Entity
@Table(name = "booking")
@NamedQueries({
        @NamedQuery(name = "Booking.findAll", query = "SELECT s FROM Booking s order by s.createDate"),
        @NamedQuery(name = "Booking.findBookingsForSeatAndFlight", query = "SELECT s FROM Booking s WHERE s.seat=:seatNr AND s.flight=:flight"),
        @NamedQuery(name = "Booking.findBookingsForFlight", query = "SELECT s FROM Booking s WHERE s.flight=:flight")
})
public class Booking extends EntityBase implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "email")
    private String email;

    @Column(name = "flight")
    private int flight;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "seat")
    private Seat seat;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "credentials")
    private Credentials credentials;

    // TODO: Remove this?
    @Column(name = "transactions")
    @OneToMany(mappedBy = "booking", cascade = CascadeType.ALL)
    private List<Transaction> transactions;

    public Booking() {
    }

    /*
     TODO: Ta bort email och customer
     */
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCustomer() {
        return email;
    }

    public void setCustomer(String email) {
        this.email = email;
    }


    public int getFlight() {
        return flight;
    }

    public void setFlight(int flight) {
        this.flight = flight;
    }


    public Seat getSeat() {
        return seat;
    }

    public void setSeat(Seat seat) {
        this.seat = seat;
    }


    public Credentials getCredentials() {
        return credentials;
    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

}
