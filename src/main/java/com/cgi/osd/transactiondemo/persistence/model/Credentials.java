package com.cgi.osd.transactiondemo.persistence.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by tomasoberg on 2017-03-21.
 */

@Entity
@Table(name = "credentials")
@NamedQueries({@NamedQuery(name = "Credentials.findAll", query = "SELECT s FROM Credentials s order by s.lastname")})
public class Credentials extends EntityBase implements Serializable {

    @Id
    private static final long serialVersionUID = 1L;

    private String firstname;
    private String lastname;
    private Integer age;
    private String street;
    private String city;
    private Integer postalCode;
    private String email;
    private String phone;

    // TODO: Put these in a separate class?
    private String creditcard_payer;
    private String creditcard;


    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(Integer postalCode) {
        this.postalCode = postalCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCreditcard_payer() {
        return creditcard_payer;
    }

    public String getCreditcard() {
        return creditcard;
    }

    public void setCreditcard_payer(String creditcard_payer) {
        this.creditcard_payer = creditcard_payer;
    }

    public void setCreditcard(String creditcard) {
        this.creditcard = creditcard;
    }

}