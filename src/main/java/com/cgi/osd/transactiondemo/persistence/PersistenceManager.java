package com.cgi.osd.transactiondemo.persistence;

import java.util.Date;
import java.util.List;

import com.cgi.osd.transactiondemo.logic.domainobject.*;
import com.cgi.osd.transactiondemo.logic.exception.PersistenceOperationException;
import com.cgi.osd.transactiondemo.persistence.model.Transaction;

/**
 * This interface defines all methods that should be used for persistence operations.
 */
public interface PersistenceManager {

    List<SeatDO> getAllSeats() throws PersistenceOperationException;

    List<AirportDO> getAllAirports() throws PersistenceOperationException;

    List<BookingDO> getAllBookings() throws PersistenceOperationException;

    boolean reserveSeat(int selectedSeat, int flight, CredentialsDO credentials) throws PersistenceOperationException;

    List<FlightDO> findFlightsBetweenDates(AirportDO departure, AirportDO destination, Date date1, Date date2) throws PersistenceOperationException;

    FlightDO findFlight(int nr) throws PersistenceOperationException;

    AirportDO getAirportById(int id) throws PersistenceOperationException;

    List<SeatDO> getSeatsWithBookingsForFlight(int flight) throws PersistenceOperationException;

    void storeTransaction(Transaction transaction) throws PersistenceOperationException;

    List<AirportDO> getAirport(String name) throws PersistenceOperationException;

}
