package com.cgi.osd.transactiondemo.persistence;

import java.io.Serializable;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import com.cgi.osd.transactiondemo.logic.exception.PersistenceOperationException;

/**
 * This class is intended to be used as an interceptor around persistence calls in order to convert exceptions from
 * persistence specific exceptions to business logic exceptions.
 */
public class ExceptionInterceptor implements Serializable {

    @Inject
    private Logger logger;

    @AroundInvoke
    public Object convertException(InvocationContext ctx) throws PersistenceOperationException {
        try {
            return ctx.proceed();

        } catch (final Exception e) {
            final String methodName = ctx.getMethod().getName();
            final PersistenceOperationException exception = handleException(e, methodName);
            throw exception;
        }
    }

    private PersistenceOperationException handleException(Throwable e, String methodName) {
        final Throwable throwableUnrolled = unrollExceptionToLast(e);
        this.logger.warning("Got exception in " + methodName + ": " + throwableUnrolled.getMessage());
        return new PersistenceOperationException(throwableUnrolled.getMessage());
    }

    private Throwable unrollExceptionToLast(Throwable exception) {
        while (exception != null && exception != exception.getCause()) {
            if (exception.getCause() == null || exception == exception.getCause()) {
                return exception;
            }
            exception = exception.getCause();
        }
        return exception;
    }


}
