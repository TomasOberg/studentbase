package com.cgi.osd.transactiondemo.persistence.model;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by tomasoberg on 2017-03-22.
 */

@Entity
@Table(name = "flight")
@NamedQueries({
        @NamedQuery(name = "Flight.findAll", query = "SELECT s FROM Flight s order by s.departureTime"),
        @NamedQuery(name = "Flight.findFlightNumber", query = "SELECT s FROM Flight s WHERE s.id = :flightNumber order by s.departureTime"),
        @NamedQuery(name = "Flight.find", query = "SELECT s FROM Flight s WHERE s.departureTime = :time AND s.departure=:departure order by s.departureTime "),
        @NamedQuery(name = "Flight.findFlightsBetweenDates", query =
                "SELECT s FROM Flight s " +
                        "WHERE s.departure = :departureAirportNr " +
                        "AND s.destination = :destinationAirportNr " +
                        "AND  s.departureTime BETWEEN :dateOfTravel AND :dateOfTravelEndOfDay " +
                        "ORDER BY s.departureTime ")
})

public class Flight extends EntityBase implements Serializable {

    @Id
    private static final long serialVersionUID = 1L;

    @Column(name = "id")
    private int id;

    @Column(name = "departure")
    private int departure;

    @Column(name = "destination")
    private int destination;

    @Column(name = "departure_time")
    private Date departureTime;

    @Column(name = "arrival_time")
    private Date arrivalTime;


    @Column(name = "price")
    private double price;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDeparture() {
        return departure;
    }

    public void setDeparture(int departure) {
        this.departure = departure;
    }

    public int getDestination() {
        return destination;
    }

    public void setDestination(int destination) {
        this.destination = destination;
    }

    public Date getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Date arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public Date getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Date departureTime) {
        this.departureTime = departureTime;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
