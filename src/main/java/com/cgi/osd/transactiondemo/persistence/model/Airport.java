package com.cgi.osd.transactiondemo.persistence.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by tomasoberg on 2017-03-21.
 */

@Entity
@Table(name = "airport")
@NamedQueries({
        @NamedQuery(name = "Airport.findAll", query = "SELECT s FROM Airport s order by s.name"),
        @NamedQuery(name = "Airport.find", query = "SELECT s FROM Airport s WHERE s.name=:airportName order by s.name"),
        @NamedQuery(name = "Airport.findByID", query = "SELECT s FROM Airport s WHERE s.id=:airportID")
})
public class Airport extends EntityBase implements Serializable {

    @Id
    private static final long serialVersionUID = 1L;

    @Column(name = "id")
    private int id;


    @Column(name = "name")
    private String name;


    public Airport() {
    }

    public Airport(String name) {
        this.name = name;
    }

    public Airport(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
