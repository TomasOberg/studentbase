package com.cgi.osd.transactiondemo.persistence.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * This class is responsible for data and methods that the entity classes have in common.
 */
@MappedSuperclass
public class EntityBase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "p_key", unique = true, nullable = false)
    private int pKey;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_date", nullable = false)
    protected Date createDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_date", nullable = false)
    protected Date updateDate;

    @Version
    @Column(name = "version", nullable = false)
    int version;

    public EntityBase() {
        super();
    }

    public Date getCreateDate() {
        return this.createDate;
    }

    public int getPKey() {
        return this.pKey;
    }

    public Date getUpdateDate() {
        return this.updateDate;
    }

    public int getVersion() {
        return this.version;
    }

    @PrePersist
    public void onPersist() {
        final Date now = new Date();
        setCreateDate(now);
        setUpdateDate(now);
    }

    @PreUpdate
    public void onUpdate() {
        final Date now = new Date();
        setUpdateDate(now);
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public void setPKey(int pKey) {
        this.pKey = pKey;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public void setVersion(int version) {
        this.version = version;
    }

}