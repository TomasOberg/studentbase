package com.cgi.osd.transactiondemo.persistence;

import java.util.Date;
import java.util.List;

import javax.ejb.*;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.TypedQuery;

import com.cgi.osd.transactiondemo.logic.domainobject.*;
import com.cgi.osd.transactiondemo.logic.exception.PersistenceOperationException;
import com.cgi.osd.transactiondemo.persistence.model.*;

/**
 * This class is responsible for implementing methods for persistence handling. All methods are thread safe.
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.NEVER)
@Interceptors(ExceptionInterceptor.class)
public class PersistenceManagerBean implements PersistenceManager {

    @Inject
    private EntityManager em;

    @Inject
    private PersistenceObjectFactory objectFactory;

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public List<SeatDO> getAllSeats() throws PersistenceOperationException {
        final List<Seat> dbResult = fetchAllSeats();
        final List<SeatDO> result = this.objectFactory.createSeatWithBookingsDOs(dbResult);
        return result;
    }

    private List<Seat> fetchAllSeats() throws PersistenceOperationException {
        final TypedQuery<Seat> query = this.em.createNamedQuery("Seat.findAll", Seat.class);
        query.setLockMode(LockModeType.OPTIMISTIC);
        final List<Seat> dbResult = query.getResultList();
        return dbResult;
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public List<AirportDO> getAllAirports() throws PersistenceOperationException {
        final List<Airport> dbResult = fetchAllAirports();
        final List<AirportDO> result = this.objectFactory.createAirportDOList(dbResult);
        return result;
    }

    private List<Airport> fetchAllAirports() throws PersistenceOperationException {
        final TypedQuery<Airport> query = this.em.createNamedQuery("Airport.findAll", Airport.class);
        query.setLockMode(LockModeType.OPTIMISTIC);
        final List<Airport> dbResult = query.getResultList();
        return dbResult;
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public List<BookingDO> getAllBookings() throws PersistenceOperationException {
        final List<Booking> dbResult = fetchAllBookings();
        final List<BookingDO> result = this.objectFactory.createBookingDOList(dbResult);
        return result;
    }
    private List<Booking> fetchAllBookings() throws PersistenceOperationException {
        final TypedQuery<Booking> query = this.em.createNamedQuery("Booking.findAll", Booking.class);
        query.setLockMode(LockModeType.OPTIMISTIC);
        final List<Booking> dbResult = query.getResultList();
        return dbResult;
    }

    private List<Credentials> fetchAllCredentials() throws PersistenceOperationException {
        final TypedQuery<Credentials> query = this.em.createNamedQuery("Credentials.findAll", Credentials.class);
        query.setLockMode(LockModeType.OPTIMISTIC);
        final List<Credentials> dbResult = query.getResultList();
        return dbResult;
    }


    private List<Flight> fetchAllFlight() throws PersistenceOperationException {
        final TypedQuery<Flight> query = this.em.createNamedQuery("Flight.findAll", Flight.class);
        query.setLockMode(LockModeType.OPTIMISTIC);
        final List<Flight> dbResult = query.getResultList();
        return dbResult;
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public boolean reserveSeat(int selectedSeat, int flight, CredentialsDO credentialsDO) throws PersistenceOperationException {

        // Find the managed seat and add a booking to it
        final TypedQuery<Seat> query = this.em.createNamedQuery("Seat.getSeat", Seat.class).setParameter("nr", selectedSeat);
        query.setLockMode(LockModeType.OPTIMISTIC);
        final Seat theSeat = query.getSingleResult();

        Booking booking = new Booking();
        booking.setSeat(theSeat);
        booking.setCustomer(credentialsDO.getFirstname());
        booking.setEmail(credentialsDO.getEmail());
        booking.setFlight(flight);

        Credentials credentials = new Credentials();
        credentials.setEmail(credentialsDO.getEmail());
        credentials.setCity(credentialsDO.getCity());
        credentials.setCreditcard(credentialsDO.getCreditcard());
        credentials.setCreditcard_payer(credentialsDO.getCreditcard_payer());
        credentials.setFirstname(credentialsDO.getFirstname());
        credentials.setLastname(credentialsDO.getLastname());
        credentials.setAge(credentialsDO.getAge());
        credentials.setPhone(credentialsDO.getPhone());
        credentials.setPostalCode(credentialsDO.getPostalCode());
        credentials.setStreet(credentialsDO.getStreet());
        booking.setCredentials(credentials);
        theSeat.addBooking(booking);
        return true;

    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public List<FlightDO> findFlightsBetweenDates(AirportDO departure, AirportDO destination, Date date1, Date date2) throws PersistenceOperationException {
        final TypedQuery<Flight> query = this.em.createNamedQuery("Flight.findFlightsBetweenDates", Flight.class)
                .setParameter("departureAirportNr", departure.getId())
                .setParameter("destinationAirportNr", destination.getId())
                .setParameter("dateOfTravel", date1)
                .setParameter("dateOfTravelEndOfDay", date2);
        query.setLockMode(LockModeType.OPTIMISTIC);
        final List<Flight> dbResult = query.getResultList();
        final List<FlightDO> result = this.objectFactory.createFlightDOList(dbResult);
        return result;
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public FlightDO findFlight(int nr) throws PersistenceOperationException {
        final TypedQuery<Flight> query = this.em.createNamedQuery("Flight.findFlightNumber", Flight.class)
                .setParameter("flightNumber", nr);

        query.setLockMode(LockModeType.OPTIMISTIC);
        final Flight dbResult = query.getSingleResult();
        final FlightDO result = this.objectFactory.createFlightDO(dbResult);
        return result;

    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public AirportDO getAirportById(int id) throws PersistenceOperationException {
        final TypedQuery<Airport> query = this.em.createNamedQuery("Airport.findByID", Airport.class)
                .setParameter("airportID", id);
        query.setLockMode(LockModeType.OPTIMISTIC);
        final Airport dbResult = query.getSingleResult();
        final AirportDO result = this.objectFactory.createAirportDO(dbResult);
        return result;
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public List<SeatDO> getSeatsWithBookingsForFlight(int flight) throws PersistenceOperationException {

        final TypedQuery<Seat> query = this.em.createNamedQuery("Seat.findAll", Seat.class);
        query.setLockMode(LockModeType.OPTIMISTIC);
        final List<Seat> seatResult = query.getResultList();
        final List<SeatDO> seatDOs = this.objectFactory.createSeatDOList(seatResult);

        //---------------------------------------------------------------------
        // Find all bookings for flight
        //---------------------------------------------------------------------
        final TypedQuery<Booking> query2 = this.em.createNamedQuery("Booking.findBookingsForFlight", Booking.class)
                .setParameter("flight", flight);

        query2.setLockMode(LockModeType.OPTIMISTIC);
        final List<Booking> bookingResult = query2.getResultList();
        final List<BookingDO> bookingDOS = this.objectFactory.createBookingDOList(bookingResult);

        //---------------------------------------------------------------------
        // For each seat, look for a corresponding booking (with the same
        // seat number) and add the booking to it
        //---------------------------------------------------------------------
        for(SeatDO seat : seatDOs) {
            for(BookingDO bookingDO : bookingDOS) {
                if(bookingDO.getSeat() == seat.getSeatNumber()) {
                    seat.addBooking(bookingDO);
                }
            }
        }
        return seatDOs;
    }
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void storeTransaction(Transaction transaction) throws PersistenceOperationException {

        em.persist(transaction);
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<AirportDO> getAirport(String name) throws PersistenceOperationException {

        final TypedQuery<Airport> query = this.em.createNamedQuery("Airport.find", Airport.class)
                .setParameter("airportName", name);


        query.setLockMode(LockModeType.OPTIMISTIC);
        final List<Airport> dbResult = query.getResultList();
        final List<AirportDO> result = this.objectFactory.createAirportDOList(dbResult);
        return result;
    }


}
