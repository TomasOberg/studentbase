package com.cgi.osd.transactiondemo.persistence;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import com.cgi.osd.transactiondemo.logic.domainobject.*;
import com.cgi.osd.transactiondemo.persistence.model.*;

/**
 * This class is responsible for creating objects by converting between domain object classes and entity classes.
 */
@ApplicationScoped
public class PersistenceObjectFactory {

    public SeatDO createSeatDO(Seat seat) {
        if (seat == null) {
            return null;
        }
        final SeatDO seatDO = new SeatDO(seat.getSeatNumber());
        return seatDO;
    }

    public SeatDO createSeatWithBookingsDO(Seat seat) {
        if (seat == null) {
            return null;
        }

        // Convert all the bookings to BookingDO objects and add it to the SeatDO
        final List<Booking> bookings = seat.getBookings();
        final List<BookingDO> bookingDOList = createBookingDOList(bookings);
        final SeatDO seatDO = new SeatDO(seat.getSeatNumber(), bookingDOList );

        return seatDO;
    }

    public List<SeatDO> createSeatDOList(Collection<Seat> seats) {
        final List<SeatDO> result = new LinkedList<>();
        for (final Seat seat : seats) {
            if (seat != null) {
                result.add(createSeatDO(seat));
            }
        }
        return result;
    }

    public List<SeatDO> createSeatWithBookingsDOs(List<Seat> seats) {
        final List<SeatDO> result = new LinkedList<>();
        for (final Seat seat : seats) {
            if (seat != null) {
                result.add(createSeatWithBookingsDO(seat));
            }
        }
        return result;
    }

    public AirportDO createAirportDO(Airport airport) {
        if (airport == null) {
            return null;
        }
        final AirportDO airportDO = new AirportDO(airport.getId(), airport.getName());
        return airportDO;
    }

    public List<AirportDO> createAirportDOList(List<Airport> airports) {
        final List<AirportDO> result = new LinkedList<>();
        for (final Airport airport : airports) {
            if (airport != null) {
                result.add(createAirportDO(airport));
            }
        }
        return result;
    }

    private CredentialsDO createCredentialsDO(Credentials credentials) {
        if (credentials == null) {
            return null;
        }
        final CredentialsDO credentialsDO = new CredentialsDO(
                credentials.getFirstname(),
                credentials.getLastname(),
                credentials.getAge(),
                credentials.getStreet(),
                credentials.getCity(),
                credentials.getPostalCode(),
                credentials.getEmail(),
                credentials.getPhone(),
                credentials.getCreditcard_payer(),
                credentials.getCreditcard()
        );
        return credentialsDO;
    }

    public List<CredentialsDO> createCredentialsDOList(List<Credentials> credentials) {
        final List<CredentialsDO> result = new LinkedList<>();
        for (final Credentials c : credentials) {
            if (c != null) {
                result.add(createCredentialsDO(c));
            }
        }
        return result;
    }

    public FlightDO createFlightDO(Flight f) {
        if (f == null) {
            return null;
        }
        final FlightDO flightDO = new FlightDO(f.getId(), f.getDeparture(), f.getDestination(), f.getDepartureTime(), f.getArrivalTime(), f.getPrice());
        return flightDO;
    }

    public List<FlightDO> createFlightDOList(List<Flight> f) {
        final List<FlightDO> result = new LinkedList<>();
        for (final Flight eachFlight : f) {
            if (eachFlight != null) {
                result.add(createFlightDO(eachFlight));
            }
        }
        return result;
    }

    private BookingDO createBookingDO(Booking booking) {
        if (booking == null)
            return null;
        final BookingDO bookingsDO = new BookingDO(booking.getFlight(), booking.getSeat().getSeatNumber(), booking.getCustomer());
        final CredentialsDO credentialsDO = createCredentialsDO(booking.getCredentials());
        bookingsDO.setCredentialsDO( credentialsDO );
        return bookingsDO;
    }

    public List<BookingDO> createBookingDOList(List<Booking> bookings) {
        final List<BookingDO> result = new LinkedList<>();
        for (final Booking b : bookings) {
            if (b != null) {
                result.add(createBookingDO(b));
            }
        }
        return result;
    }
}
