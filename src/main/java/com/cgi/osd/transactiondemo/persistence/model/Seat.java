package com.cgi.osd.transactiondemo.persistence.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

/**
 * The persistence class for the seat database table.
 */
@Entity
@Table(name = "seat")
@NamedQueries({
        @NamedQuery(name = "Seat.findAll", query = "SELECT s FROM Seat s order by s.seatNumber"),
        @NamedQuery(name = "Seat.getSeat", query = "SELECT s FROM Seat s WHERE s.seatNumber=:nr")
})
public class Seat extends EntityBase implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "seat_number", nullable = false, unique = true)
    private int seatNumber;

    @Column(name = "booking")
    @OneToMany(mappedBy = "seat", cascade= CascadeType.ALL)
    private List<Booking> bookings;


    public Seat() {
        bookings = new ArrayList<>();
    }

    public Seat(int seatNumber) {
        this.seatNumber = seatNumber;
    }

    public int getSeatNumber() {
        return this.seatNumber;
    }

    public void setSeatNumber(int seatNumber) {
        this.seatNumber = seatNumber;
    }

    public void addBooking(Booking booking) {
        bookings.add(booking);
    }

    public List<Booking> getBookings() {
        return bookings;
    }

    public void setBookings(List<Booking> bookings) {
        this.bookings = bookings;
    }


}