import {Injectable}       from '@angular/core';
import {Http}             from '@angular/http';
import {Headers}          from '@angular/http';

import 'rxjs/add/operator/toPromise';

import {ErrorMessage} from "../model/errormessage";
import {Message} from "primeng/components/common/api";
import {environment} from "../../environments/environment";

import {Credentials}      from '../model/credentials';
import {Seat}             from '../model/seat';
import {Flight}           from '../model/flight';
import {Airport}          from '../model/airport';


@Injectable()
export class BookingService {

    // URL used for the Rest Service
    // baseUrl: string = "http://localhost:8080/TicketBooking/rest/FlightBookingService";

    baseUrl: string = environment.production?
    location.origin + "/TicketBooking/rest/FlightBookingService":
    "http://localhost:8080/TicketBooking/rest/FlightBookingService";

    private headers = new Headers({'Content-Type': 'application/json'});

    constructor(private http: Http) { }

    private handleServerError(error: any, msgs: Message[]): Promise<any> {
      const errorMessage: ErrorMessage = error.json();
      console.log(errorMessage.message);
      msgs = [];
      msgs.push({severity: 'warn', summary: 'Server Error', detail: errorMessage.message});
      return Promise.reject(error.message || error);
    }

    private handleError(error: any): Promise<any> {
        console.log("STOP!");
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }

/**
     * get all available Airport objects
     * @returns {Promise<TResult|TResult>}
     */
    getAirports() {
        return this.http.get(this.baseUrl + "/getAirports")
            .toPromise()
            .then(res => <Airport[]> res.json().airports)
            .then(data => {
                //console.log("fick:");
                //console.log(data);

                return data;
            }).catch(this.handleError);
    }


    getSeatsForFlight(id: number) {
        return this.http.get(this.baseUrl + "/getSeatsForFlight/" + id)
            .toPromise()
            .then(res => <Seat[]> res.json().seats)
            .then(data => {
                console.log("fick:");
                console.log(data);
                return data;
            }).catch(this.handleError);
    }

    getAvailableFlights(departure: number, destination: number, departureStartDate: Date, departureEndRangeDate: Date) {
        // Create a JSON object with the prerequsitive
        const req = {
            'departureDate': departureStartDate,
            'arrivalDate': departureEndRangeDate,
            'departure': departure,
            'destination': destination
        };
        console.log(" REQ: " + departure + " till " + destination + " " + departureStartDate);
        console.log("Fetching flights, please wait!");

        return this.http.post(this.baseUrl + "/getAvailableFlights", req, {headers: this.headers})
            .toPromise()
            .then(res => <Flight[]> res.json().flights)
            .then(data => {
                console.log("fick:")
                console.log(data);
                return data;
            }).catch(this.handleError);
    }

    /**
     * Make a reservation for flight and seat. Add credentials
     * @param flightNr
     * @param seatNr
     * @param credentials
     * @returns {Promise<TResult|TResult>}
     */

    makeReservation(flightNr: number, seatNr: number, credentials: Credentials, price: string) {
        const req = {
            "flight": flightNr,
            "seatNr": seatNr,
            "credentials": credentials,
            "price": price,
        };

        return this.http.post(this.baseUrl + "/makeReservation", req, {headers: this.headers})
            .toPromise()
            .then(res => res.json())
            .then(data => {
                console.log("fick:");
                console.log(data);
                return data;
            }).catch(this.handleError);
    }

}
