/**
 * Flightbooking router,
 * Defines routes to be taken across the app
 */
import { NgModule }                 from '@angular/core';
import { RouterModule, Routes }     from '@angular/router';
import { AppComponent }             from "./components/app.component";
import { FlightBookingComponent }   from "./components/flightbooking/app.flightbooking.component";
import {ConfirmationPageComponent} from "./components/confirmation-page/confirmation-page.component";


const routes: Routes = [
    { path: '', component: AppComponent },
    { path: 'FlightBooking', component: FlightBookingComponent },
    { path: 'Confirmation', component: ConfirmationPageComponent }, // TODO: Replace with SuccessComponent or something
];
@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}
