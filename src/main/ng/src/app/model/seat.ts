import {Booking} from '../../app/model/booking';

/**
 * Seat object
 */
export class Seat {
    seatNumber: number;
    reservationStatus: boolean;         // TODO: Remove this (everywhere, should not be part of the domain model anymore)
    bookings: Booking[];
}