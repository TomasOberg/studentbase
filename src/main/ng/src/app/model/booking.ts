import { Credentials } from '../../app/model/credentials';


/**
 * Booking object
 */
export class Booking 
{
 flight: number;
 customer: string;
 seat: number;
 credentialsDO?: Credentials;
}


