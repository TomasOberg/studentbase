/**
 * Flight object
 */
export class Flight {
  id: number;
  departureTime: Date;
  arrivalTime: Date;
  departure:number;
  destination:number;
  departureTimeFormatted: string;
  arrivalTimeFormatted: string;

  price: string;

  /*
  public constructor( id: number, departure:number,   destination:number, departureTime: Date, arrivalTime: Date  ){
        this.departure = departure;
        this.destination = destination;
        this.departureTime = departureTime;
        this.arrivalTime = arrivalTime;
    }*/



  // public FlightDO(int id, Date departureTime, Date arrivalTime, int departure, int destination, double price,
  // String departureTimeFormatted, String arrivalTimeFormatted)


  public constructor( id: number,  departureTime: Date, arrivalTime: Date, departure:number, destination:number, price: string,
                      departureTimeFormatted: string, arrivalTimeFormatted: string )
  {
    this.departure = departure;
    this.destination = destination;
    this.departureTime = departureTime;
    this.arrivalTime = arrivalTime;

    this.price = price;

    this.departureTimeFormatted = departureTimeFormatted;
    this.arrivalTimeFormatted = arrivalTimeFormatted;

  }


}

