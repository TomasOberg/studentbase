export class ErrorMessage {
  public errorCode: number;
  public message: string;
}