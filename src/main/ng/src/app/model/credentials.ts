/**
 * Credentials object
 */
export class Credentials
{
    firstname: string;
    lastname: string;
    age: number;
    street: string;
    city: string;
    postalCode: number;
    email: string;
    phone: string;
    creditcard_payer: string;
    creditcard: string;
    customer: string;

    constructor() {}
}


