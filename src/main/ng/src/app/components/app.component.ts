import {Component, OnInit}                          from '@angular/core';
import {Router}                                     from '@angular/router';

// TODO: rename this component
@Component({
    selector: 'app-root',
    styleUrls: ['./app.component.css'],
    template: `
        <img src="/assets/alleviate.png" class="logo" />
        <router-outlet></router-outlet>
    `
})
export class AppComponent implements OnInit {
    constructor(private router: Router) {
    }
    ngOnInit() {
        this.router.navigate(['/FlightBooking']);
    }
}
