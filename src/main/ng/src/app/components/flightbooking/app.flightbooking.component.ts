import { Component, OnInit }       from '@angular/core';
import { Message }                 from 'primeng/primeng';
import { DataService } from "../../services/data.service";

@Component({
  selector: 'app-flight-booking',
  templateUrl: './app.flightbooking.component.html',
  styleUrls: ['./app.flightbooking.component.css'],
})
export class FlightBookingComponent implements OnInit {

  constructor(private ds: DataService) {
    this.ds.clearAll();
  }
  ngOnInit() {}
  onClickTab(e) {}

  get msgs(): Message[] { return this.ds.msgs; }
  set msgs(msgs: Message[] ) { this.ds.msgs = msgs; }

  get selectedTab(): number {
    return this.ds.selectedTab;
  }

  set selectedTab(value: number) {
    this.ds.selectedTab = value;
  }

}
