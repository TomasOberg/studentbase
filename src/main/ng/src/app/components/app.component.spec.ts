import { TestBed } from '@angular/core/testing';

import { FlightBookingComponent } from './flightbooking/app.flightbooking.component';

describe('App', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({ declarations: [FlightBookingComponent]});
  });

  it ('should work', () => {
    let fixture = TestBed.createComponent(FlightBookingComponent);
    expect(fixture.componentInstance instanceof FlightBookingComponent).toBe(true, 'should create AppComponent');
  });
});
