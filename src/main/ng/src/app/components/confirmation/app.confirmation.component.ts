import {Component, Input} from '@angular/core';
import { Router } from '@angular/router';
import {Credentials} from "../../model/credentials";
import {DataService} from "../../services/data.service";
import {FormGroup} from "@angular/forms";
import {BookingService} from "../../services/booking.service";
import {Airport} from "../../model/airport";
import {Flight} from "../../model/flight";
import {Seat} from "../../model/seat";

// TODO: Se till att man inte kan nå confirmation-sidan innan en bokning är klar alternativt att man inte kan nå den manuellt

/**
 *
 * Main App component - bindings with HTML found in the app.component.flightbooking.component.html
 */
@Component({
    selector: 'app-confirmation',
    templateUrl: './app.confirmation.component.html',
    styleUrls: ['./app.confirmation.component.css'],
})
export class ConfirmationComponent  {

  constructor(
    private bookingService: BookingService,
    private router: Router,
    private ds: DataService) {

  }
  get bookingFormPayment(): FormGroup {return this.ds.bookingFormPayment;}
  get bookingFormCredentials(): FormGroup {return this.ds.bookingFormCredentials;}
  get selectedSeat(): Seat {return this.ds.selectedSeat;  }
  get departure(): Airport {return this.ds.departure;}
  get destination(): Airport {return this.ds.destination;}
  get firstName(): string {return this.ds.firstName;  }
  get lastName(): string {return this.ds.lastName;  }
  get city(): string {return this.ds.city;}
  get email(): string {return this.ds.email;}
  get address(): string {return this.ds.address;  }
  get postalNr(): string {return this.ds.postalNr;}
  get selectedFlight(): Flight {return this.ds.selectedFlight;  }
  get payerName(): string {return this.ds.payerName;}
  get creditcardNumber(): string {return this.ds.creditcardNumber;}
  get departureID(): number {return this.ds.departureID;}
  get destinationID(): number {return this.ds.destinationID;}


  makeReservation() {

    // TODO: Skapa credential i ds och returnera hela det objektet hit istället.
    const credentials = new Credentials();
    credentials.firstname = this.ds.bookingFormCredentials.get("firstName").value;
    credentials.lastname = this.ds.bookingFormCredentials.get("lastName").value;
    credentials.street = this.ds.address;
    credentials.city = this.ds.city;
    credentials.postalCode = +this.ds.postalNr;
    credentials.email = this.ds.bookingFormCredentials.get("email").value;
    credentials.creditcard = this.ds.bookingFormPayment.get("creditcard").value;
    credentials.creditcard_payer = this.ds.bookingFormPayment.get("payer").value;




    this.bookingService.makeReservation(this.ds.selectedFlight.id , this.ds.selectedSeat.seatNumber, credentials, this.ds.selectedFlight.price)
      .then(result => {
        // Plocka ut text från _body
        console.log( result.responseText );
        // TODO: Add a booking confirmation page
        this.router.navigate(['/Confirmation']);
        //this.ds.clearAll();
      });
    }

  goBackTab() {
    this.ds.previousTab();
  }


}
