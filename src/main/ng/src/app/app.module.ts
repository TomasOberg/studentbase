import 'rxjs/add/operator/toPromise';

import { NgModule }                                             from '@angular/core';
import { FormsModule, ReactiveFormsModule }                     from '@angular/forms';
import { HttpModule }                                           from '@angular/http';
import { RouterModule }                                         from '@angular/router';
import { BrowserModule }                                        from '@angular/platform-browser';
import { BrowserAnimationsModule }                              from '@angular/platform-browser/animations';

import { TabViewModule, PanelModule, CalendarModule } from 'primeng/primeng';
import { DataListModule, AutoCompleteModule, DataGridModule } from 'primeng/primeng';
import { MessagesModule, InputTextModule, DataTableModule, GrowlModule } from 'primeng/primeng';
import { ButtonModule, DialogModule, InputMaskModule, ListboxModule, DropdownModule } from 'primeng/primeng';


import { AppComponent } from './components/app.component';
import { AppRoutingModule } from './app.routing.module';

import { FlightBookingComponent } from './components/flightbooking/app.flightbooking.component';
import { ConfirmationComponent } from './components/confirmation/app.confirmation.component';

import { BookingService } from './services/booking.service';
import { DataService } from './services/data.service';
import { ValidationService } from './services/validation.service';


import { APP_BASE_HREF } from "@angular/common";
import { environment } from "../environments/environment";
import { TraveldetailsComponent } from './components/traveldetails/traveldetails.component';
import { SeatpickingComponent } from './components/seatpicking/seatpicking.component';
import { CredentialsComponent } from './components/credentials/credentials.component';
import { PaymentComponent } from './components/payment/payment.component';
import { ConfirmationPageComponent } from './components/confirmation-page/confirmation-page.component';

@NgModule({

  imports: [
    BrowserModule,
    FormsModule, HttpModule, InputTextModule, DataTableModule, ButtonModule, DialogModule, TabViewModule,
    PanelModule, CalendarModule, DataListModule, AutoCompleteModule, DataGridModule, ReactiveFormsModule,
    MessagesModule, RouterModule, AppRoutingModule, InputMaskModule, ListboxModule, DropdownModule, GrowlModule,
    BrowserAnimationsModule
  ],
  exports: [ RouterModule ],
  declarations: [
      AppComponent,
      FlightBookingComponent,
      ConfirmationComponent,
      TraveldetailsComponent,
      SeatpickingComponent,
      CredentialsComponent,
      PaymentComponent,
      ConfirmationPageComponent
  ],
  bootstrap: [ AppComponent ],
  providers: [
    BookingService,
    DataService,
    ValidationService,
    { provide: APP_BASE_HREF, useValue: environment.production ? "/TicketBooking/ng/" : "/" }
  ]

})
export class AppModule {


}



