import { AngularbookPage } from './app.po';

describe('angularbook App', () => {
  let page: AngularbookPage;

  beforeEach(() => {
    page = new AngularbookPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
