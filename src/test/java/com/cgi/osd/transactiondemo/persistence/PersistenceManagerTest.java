package com.cgi.osd.transactiondemo.persistence;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.List;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.*;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.cgi.osd.transactiondemo.logic.domainobject.BookingDO;
import com.cgi.osd.transactiondemo.persistence.model.Booking;
import com.cgi.osd.transactiondemo.persistence.model.Credentials;
import com.cgi.osd.transactiondemo.persistence.model.Seat;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;
import com.cgi.osd.transactiondemo.logic.exception.PersistenceOperationException;

/**
 * This class is responsible for testing the persistence manager.
 *
 */
@RunWith(Arquillian.class)
public class PersistenceManagerTest {

    @Deployment
    public static Archive<WebArchive> createTestArchive() {
		final WebArchive archive = ShrinkWrap.create(WebArchive.class, "persistence-manager-test.war");
		archive.addPackages(true, "com.cgi.osd.transactiondemo.logic");
		archive.addPackages(true, "com.cgi.osd.transactiondemo.persistence");
		archive.addPackages(true, "com.cgi.osd.transactiondemo.util");

        archive.addPackages(true, "com.cgi.osd.transactiondemo.");

        archive.deleteClasses(PersistenceObjectFactoryTest.class);
		archive.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml");
		archive.addAsWebInfResource("test-ds.xml");
		archive.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
		return archive;
    }

    private final int NUMBER_OF_SEATS = 100;

    @Inject
    private PersistenceManager persistenceManager;

    @PersistenceContext(unitName = "primary")
    private EntityManager em;

    @Inject
    UserTransaction utx;

    @Before
    public void initDataBase() {
		try {
			this.utx.begin();
			this.em.joinTransaction();
			emptySeatTable();
			addSeatsToSeatTable();

			addBookings();

			this.utx.commit();
		} catch (final NotSupportedException e) {
			fail("NotSupportedException: " + e.getMessage());
		} catch (final SystemException e) {
			fail("SystemException: " + e.getMessage());
		} catch (final SecurityException e) {
			fail("SecurityException: " + e.getMessage());
		} catch (final IllegalStateException e) {
			fail("IllegalStateException: " + e.getMessage());
		} catch (final RollbackException e) {
			fail("RollbackException: " + e.getMessage());
		} catch (final HeuristicMixedException e) {
			fail("HeuristicMixedException: " + e.getMessage());
		} catch (final HeuristicRollbackException e) {
			fail("HeuristicRollbackException: " + e.getMessage());
		}
    }


    @Test
    public void testGetAllSeats() throws PersistenceOperationException {
		final List<SeatDO> seats = this.persistenceManager.getAllSeats();
		for (int i = 0; i < this.NUMBER_OF_SEATS; i++) {
			final SeatDO seat = seats.get(i);
			assertEquals(i, seat.getSeatNumber());
		}
    }

    @Test
    public void testInjects() {
		assertNotNull(this.em);
		assertNotNull(this.persistenceManager);
		assertNotNull(this.utx);
    }

    private void addSeatsToSeatTable() {
		final String sqlInsertString = "INSERT INTO `seat` (create_date, update_date, version, seat_number) VALUES(NOW(), NOW(), 0, :seatNumber)";
		final Query query = this.em.createNativeQuery(sqlInsertString);
		for (int i = 0; i < this.NUMBER_OF_SEATS; i++) {
            query.setParameter("seatNumber", i);
            query.executeUpdate();
       }
    }
	private void emptySeatTable() {
		final String deleteString = "DELETE FROM `seat`";
		this.em.createNativeQuery(deleteString).executeUpdate();

	}

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    private void addBookings() {

        // Find the managed seat and add a booking to it
        final TypedQuery<Seat> query = this.em.createNamedQuery("Seat.getSeat", Seat.class).setParameter("nr", 3);
        query.setLockMode(LockModeType.OPTIMISTIC);
        final Seat theSeat = query.getSingleResult();

        Booking booking = new Booking();
        booking.setSeat(theSeat);
        booking.setEmail("tomaserikoberg@gmail.com");
        booking.setFlight(3);

        Credentials credentials = new Credentials();
        credentials.setEmail(booking.getEmail());
        credentials.setCity("Östersund");
        credentials.setCreditcard("1234-1234-5633-2342");
        credentials.setCreditcard_payer("Tomas");
        credentials.setFirstname("Tomas");
        credentials.setLastname("Öberg");
        credentials.setAge(36);
        credentials.setPhone("0706821464");
        credentials.setPostalCode(12345);
        credentials.setStreet("Östersund");

        booking.setCredentials(credentials);

        theSeat.addBooking(booking);

        em.persist(theSeat);
    }



	@Test
	public void testGetAllBookings() throws PersistenceOperationException {
		//final List<BookingDO> bookings = persistenceManager.getAllBookings();
        //final Query nativeQuery = em.createNativeQuery("SELECT * FROM demo_db.bookings");
        //nativeQuery.getResultList();


        //addBookings();

        final List<BookingDO> allBookings =persistenceManager.getAllBookings();
        assertEquals(1, allBookings);
	}




	/*
	public List<CredentialsDO> getAllCredentials() throws PersistenceOperationException {
		return null;
	}

	public List<FlightDO> getAllFlight() throws PersistenceOperationException {
		return null;
	}

	public boolean reserveSeat(int selectedSeat, int flight, CredentialsDO credentials) throws PersistenceOperationException {
		return false;
	}


	public List<FlightDO> getFlights(Date date, int departure) throws PersistenceOperationException {
		return null;
	}

	public List<BookingDO> getBookingsForSeatAndFlight(SeatDO seatDO, FlightDO flightDO) throws PersistenceOperationException {
		return null;
	}

	public List<BookingDO> getBookingsForSeatAndFlight(int seatNr, int flightNr) throws PersistenceOperationException {
		return null;
	}


	public List<BookingDO> getBookingsForFlight(int flightNr) throws PersistenceOperationException {
		return null;
	}

	public List<FlightDO> findFlightsBetweenDates(AirportDO departure, AirportDO destination, Date date1, Date date2) throws PersistenceOperationException {
		return null;
	}

	public List<FlightDO> findFlightsBetweenDates(int departure, int destination, Date date1, Date date2) throws PersistenceOperationException {
		return null;
	}

	public FlightDO findFlight(int nr) throws PersistenceOperationException {
		return null;
	}


	public AirportDO getAirport(String destination) throws PersistenceOperationException {
		return null;
	}

	public AirportDO getAirportById(int id) throws PersistenceOperationException {

		//bookingService.findFlightsBetweenDates(123,)
		return null;
	}

	public List<SeatDO> getSeatsWithBookingsForFlight(int flight) throws PersistenceOperationException {
		return null;
	}


    */



    /*
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void testBook() throws PersistenceOperationException {

		// Find the managed seat and add a booking to it
		final TypedQuery<Seat> query = this.em.createNamedQuery("Seat.getSeat", Seat.class).setParameter("nr", 5);
		query.setLockMode(LockModeType.OPTIMISTIC);
		final Seat theSeat = query.getSingleResult();


		theSeat.setReserved(true);

		Booking booking = new Booking();
		booking.setSeat(theSeat);
		booking.setFlight(123456);
		booking.setEmail("muppen!");
		theSeat.addBooking(booking);
	}*/

}
