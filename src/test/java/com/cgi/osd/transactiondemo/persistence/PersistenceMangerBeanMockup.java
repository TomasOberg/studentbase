package com.cgi.osd.transactiondemo.persistence;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Alternative;
import javax.inject.Inject;

import com.cgi.osd.transactiondemo.logic.BookingService;
import com.cgi.osd.transactiondemo.logic.domainobject.*;
import com.cgi.osd.transactiondemo.logic.exception.PersistenceOperationException;
import com.cgi.osd.transactiondemo.persistence.model.Airport;
import com.cgi.osd.transactiondemo.persistence.model.Seat;
import com.cgi.osd.transactiondemo.persistence.model.Transaction;

/**
 * This class is responsible for constituting a test implementation of the PersistenceManager interface.
 */
@Alternative
@ApplicationScoped
public class PersistenceMangerBeanMockup implements PersistenceManager {

    @Inject
    private BookingService bookingService;

    @Inject
    private PersistenceObjectFactory objectFactory;

    @Override
    public List<SeatDO> getAllSeats() throws PersistenceOperationException {
        final Collection<Seat> seats = new LinkedList<>();
        for (int i = 0; i < 100; i++) {
            seats.add(new Seat(i));
        }

        final List<SeatDO> seatDOs = this.objectFactory.createSeatDOList(seats);
        return seatDOs;
    }

    @Override
    public List<AirportDO> getAllAirports() throws PersistenceOperationException {
        final List<Airport> airports = new LinkedList<>();
        airports.add(new Airport(1001, "hej1"));
        airports.add(new Airport(1002, "hej2"));
        airports.add(new Airport(1003, "hej3"));
        airports.add(new Airport(1004, "hej4"));
        airports.add(new Airport(1005, "hej5"));
        airports.add(new Airport(1006, "hej6"));

        final List<AirportDO> airportDOs = this.objectFactory.createAirportDOList(airports);
        return airportDOs;
    }

    @Override
    public List<BookingDO> getAllBookings() throws PersistenceOperationException {


        return null;
    }



    @Override
    public boolean reserveSeat(int selectedSeat, int flight, CredentialsDO credentials) throws PersistenceOperationException {
        return false;
    }


    @Override
    public List<FlightDO> findFlightsBetweenDates(AirportDO departure, AirportDO destination, Date date1, Date date2) throws PersistenceOperationException {
        return null;
    }




    @Override
    public FlightDO findFlight(int nr) throws PersistenceOperationException {
        return null;
    }



    @Override
    public AirportDO getAirportById(int id) throws PersistenceOperationException {
        return null;
    }

    @Override
    public List<SeatDO> getSeatsWithBookingsForFlight(int flight) throws PersistenceOperationException {
        return null;
    }


    @Override
    public void storeTransaction(Transaction transaction) throws PersistenceOperationException {
        return;
    }

    @Override
    public List<AirportDO> getAirport(String name) throws PersistenceOperationException {
        return null;
    }

    /*
    @Override
    public List<FlightDO> getAllFlight() throws PersistenceOperationException {
        return null;
    }
    @Override
    public List<FlightDO> getFlights(Date date, int departure) throws PersistenceOperationException {
        return null;
    }

    @Override
    public List<BookingDO> getBookingsForSeatAndFlight(SeatDO seatDO, FlightDO flightDO) throws PersistenceOperationException {
        return null;
    }
    @Override
    public List<FlightDO> findFlightsBetweenDates(int departure, int destination, Date date1, Date date2) throws PersistenceOperationException {
        return null;
    }
    @Override
    public List<EntityBase> rawQuery(String query) throws PersistenceOperationException {
        return null;
    }
    @Override
    public List<CredentialsDO> getAllCredentials() throws PersistenceOperationException {
        return null;
    }


    @Override
    public List<BookingDO> getBookingsForSeatAndFlight(int seatNr, int flightNr) throws PersistenceOperationException {
        return null;
    }


    @Override
    public List<BookingDO> getBookingsForFlight(int flightNr) throws PersistenceOperationException {
        return null;
    }

    @Override
    public AirportDO getAirport(String destination) throws PersistenceOperationException {
        return null;
    }
    */

}
