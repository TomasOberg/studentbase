
/*
  TODO: Se över vilka som ska vara NOT NULL eller inte
 */


BEGIN;

CREATE DATABASE IF NOT EXISTS `demo_db` DEFAULT CHARACTER SET utf8 COLLATE utf8_swedish_ci;
USE `demo_db`;


DROP TABLE if EXISTS `demo_db`.`booking`, `demo_db`.`credentials`, `demo_db`.`flight`, `demo_db`.`seat` CASCADE;
DROP TABLE if EXISTS `demo_db`.`airport`;

DROP TABLE IF EXISTS `seat`;
CREATE TABLE `seat` (
  `p_key` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `version` INT(11) NOT NULL,
  `seat_number` INT(11) UNIQUE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;


DROP TABLE IF EXISTS `airport`;
CREATE TABLE `airport` (
  `p_key` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `version` int(11) NOT NULL,
  `id` int(11) NOT NULL UNIQUE,
  `name` VARCHAR(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;


DROP TABLE IF EXISTS `flight`;
CREATE TABLE `flight` (
  `p_key` int(11) NOT NULL AUTO_INCREMENT,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `version` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `departure` int(11) NOT NULL,
  `destination` int(11) NOT NULL,
  `departure_time` datetime NOT NULL,
  `arrival_time` datetime NOT NULL,
  `price` DOUBLE NOT NULL,
  CONSTRAINT FOREIGN KEY (departure) REFERENCES demo_db.airport(id) MATCH SIMPLE,
  CONSTRAINT FOREIGN KEY (destination) REFERENCES demo_db.airport(id) MATCH SIMPLE,
  PRIMARY KEY (`p_key`),
  UNIQUE KEY `UNIQUE_FLIGHT_NUMBER` (`id`),
  KEY `INDEX_FLIGHT_NUMBER` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;


DROP TABLE IF EXISTS `credentials`;
CREATE TABLE `credentials` (
  `p_key` int(11) NOT NULL AUTO_INCREMENT,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `version` int(11) NOT NULL,
  `firstname` VARCHAR(255) NOT NULL,
  `lastname` VARCHAR(255) NOT NULL,
  `age` INT(11),
  `street` VARCHAR(255),
  `city` VARCHAR(255),
  `postalCode` INT(11),
  `email` VARCHAR(255) NOT NULL,
  `phone` VARCHAR(255),
  `creditcard_payer` VARCHAR(255) NOT NULL,
  `creditcard` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`p_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;



DROP TABLE IF EXISTS `transaction`;
CREATE TABLE `transaction` (
  `p_key` int(11) NOT NULL AUTO_INCREMENT,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `version` int(11) NOT NULL,
  `email`             VARCHAR(255) NOT NULL,
  `transaction_time`  BIGINT,
  `request_id`        VARCHAR(255),
  `transaction_id`    VARCHAR(255),
  `amount`            DOUBLE,
  `booking`           INT(11),    /* TODO: Remove */
  PRIMARY KEY (`p_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

DROP TABLE IF EXISTS `booking`;
CREATE TABLE `booking` (
  `p_key` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `version` INT(11) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `flight` INT(11) NOT NULL,
  `seat` INT(11),
  `credentials` INT(11) DEFAULT NULL,
  `transactions` INT(11) DEFAULT NULL,    /* TODO: Remove */
  CONSTRAINT FOREIGN KEY (seat) REFERENCES demo_db.seat(p_key)
    MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT FOREIGN KEY (credentials) REFERENCES demo_db.credentials(p_key)
    MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT FOREIGN KEY (transactions) REFERENCES demo_db.transaction(p_key)
    MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION
  /* CONSTRAINT FOREIGN KEY (flight) REFERENCES demo_db.flight(p_key) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION */

) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;



DROP PROCEDURE IF EXISTS `init_seat`;
DELIMITER $$
CREATE PROCEDURE init_seat()
BEGIN
	DECLARE seat_number INT DEFAULT 1 ;
	INIT_LOOP: LOOP
		INSERT INTO `seat` (create_date, update_date, version, seat_number) VALUES(NOW(), NOW(), 0, seat_number);
		SET seat_number = seat_number + 1;
		IF seat_number = 101 THEN
			LEAVE INIT_LOOP;
		END IF;
	END LOOP INIT_LOOP;
END $$
DELIMITER ;
CALL `init_seat`;
DROP PROCEDURE `init_seat`;
COMMIT;


DROP PROCEDURE IF EXISTS `init_airport`;
DELIMITER $$
CREATE PROCEDURE init_airport()
  BEGIN
    INSERT INTO `airport` (create_date, update_date, version, id, name) VALUES(NOW(), NOW(), 0, 1,"Stockholm");
    INSERT INTO `airport` (create_date, update_date, version, id, name) VALUES(NOW(), NOW(), 0, 2,"Göteborg");
    INSERT INTO `airport` (create_date, update_date, version, id, name) VALUES(NOW(), NOW(), 0, 3,"Malmö");
    INSERT INTO `airport` (create_date, update_date, version, id, name) VALUES(NOW(), NOW(), 0, 123,"Östersund");


    INSERT INTO `airport` (create_date, update_date, version, id, name) VALUES(NOW(), NOW(), 0, 400,"Luleå");

  END $$
DELIMITER ;
CALL `init_airport`;
DROP PROCEDURE `init_airport`;
COMMIT;


DROP PROCEDURE IF EXISTS `init_flight`;
DELIMITER $$
CREATE PROCEDURE init_flight()
  BEGIN
    INSERT INTO `flight` (create_date, update_date, version, id, price, departure, destination, departure_time, arrival_time) VALUES(NOW(), NOW(), 0, 1,  500.00, 123,1,"2017-03-29 14:15:00","2017-03-29 16:15:00");
    INSERT INTO `flight` (create_date, update_date, version, id, price, departure, destination, departure_time, arrival_time) VALUES(NOW(), NOW(), 0, 2,  700.00, 123,3,"2017-03-29 12:15:00","2017-03-29 15:30:00");
    INSERT INTO `flight` (create_date, update_date, version, id, price, departure, destination, departure_time, arrival_time) VALUES(NOW(), NOW(), 0, 3,  500.00, 123,1,"2017-03-30 14:15:00","2017-03-30 16:15:00");
    INSERT INTO `flight` (create_date, update_date, version, id, price, departure, destination, departure_time, arrival_time) VALUES(NOW(), NOW(), 0, 5,  700.00, 123,2,"2017-03-31 16:45:00","2017-03-31 18:15:00");
    INSERT INTO `flight` (create_date, update_date, version, id, price, departure, destination, departure_time, arrival_time) VALUES(NOW(), NOW(), 0, 6,  600.00, 123,3,"2017-03-31 12:15:00","2017-03-31 15:30:00");
    INSERT INTO `flight` (create_date, update_date, version, id, price, departure, destination, departure_time, arrival_time) VALUES(NOW(), NOW(), 0, 7,  800.00, 123,1,"2017-03-31 14:15:00","2017-03-31 15:15:00");
    INSERT INTO `flight` (create_date, update_date, version, id, price, departure, destination, departure_time, arrival_time) VALUES(NOW(), NOW(), 0, 8,  500.00, 123,1,"2017-03-31 16:15:00","2017-03-31 17:15:00");
    INSERT INTO `flight` (create_date, update_date, version, id, price, departure, destination, departure_time, arrival_time) VALUES(NOW(), NOW(), 0, 9,  660.00, 123,1,"2017-03-31 18:15:00","2017-03-31 19:15:00");
    INSERT INTO `flight` (create_date, update_date, version, id, price, departure, destination, departure_time, arrival_time) VALUES(NOW(), NOW(), 0, 10, 670.00, 123,1,"2017-03-31 20:15:00","2017-03-31 21:15:00");



    INSERT INTO `flight` (create_date, update_date, version, id, price, departure, destination, departure_time, arrival_time) VALUES(NOW(), NOW(), 0, 11,  240.00, 1,400,"2017-05-25 14:35:00","2017-05-25 16:35:00");
    INSERT INTO `flight` (create_date, update_date, version, id, price, departure, destination, departure_time, arrival_time) VALUES(NOW(), NOW(), 0, 12,  723.00, 1,400,"2017-05-25 12:35:00","2017-05-25 15:30:00");
    INSERT INTO `flight` (create_date, update_date, version, id, price, departure, destination, departure_time, arrival_time) VALUES(NOW(), NOW(), 0, 13,  556.00, 1,400,"2017-05-25 14:35:00","2017-05-25 16:35:00");
    INSERT INTO `flight` (create_date, update_date, version, id, price, departure, destination, departure_time, arrival_time) VALUES(NOW(), NOW(), 0, 15,  764.00, 1,400,"2017-05-25 16:35:00","2017-05-25 18:35:00");
    INSERT INTO `flight` (create_date, update_date, version, id, price, departure, destination, departure_time, arrival_time) VALUES(NOW(), NOW(), 0, 16,  260.00, 1,400,"2017-05-25 12:35:00","2017-05-25 15:30:00");
    INSERT INTO `flight` (create_date, update_date, version, id, price, departure, destination, departure_time, arrival_time) VALUES(NOW(), NOW(), 0, 17,  284.00, 1,400,"2017-05-25 14:35:00","2017-05-25 15:35:00");
    INSERT INTO `flight` (create_date, update_date, version, id, price, departure, destination, departure_time, arrival_time) VALUES(NOW(), NOW(), 0, 18,  255.00, 1,400,"2017-05-25 16:35:00","2017-05-25 17:35:00");
    INSERT INTO `flight` (create_date, update_date, version, id, price, departure, destination, departure_time, arrival_time) VALUES(NOW(), NOW(), 0, 19,  363.00, 1,400,"2017-05-25 18:35:00","2017-05-25 19:35:00");
    INSERT INTO `flight` (create_date, update_date, version, id, price, departure, destination, departure_time, arrival_time) VALUES(NOW(), NOW(), 0, 20,  562.00, 1,400,"2017-05-25 20:35:00","2017-05-25 21:35:00");

  END $$
DELIMITER ;
CALL `init_flight`;
DROP PROCEDURE `init_flight`;
COMMIT;
